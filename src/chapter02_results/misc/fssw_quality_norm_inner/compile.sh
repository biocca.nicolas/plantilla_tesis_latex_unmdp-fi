#!/bin/bash

BUILD=build

# compile tex 
pdflatex -file-line-error -interaction=nonstopmode $BUILD.tex
rm $BUILD.{aux,bbl,blg,synctex.gz,out,auxlock}


# rename it
folderName=${PWD##*/}
mv $BUILD.pdf $folderName.pdf

# if multi-page figure
NB_PAGES=$(pdfinfo $folderName.pdf | grep Pages | awk '{print $2}')
if [ $NB_PAGES -gt 1 ]
then
  echo info: multi-page pdf found...
  echo "info: I'll split multi-page pdf file into individual images."
  pdfseparate ${folderName}.pdf ${folderName}_%d.pdf
fi
