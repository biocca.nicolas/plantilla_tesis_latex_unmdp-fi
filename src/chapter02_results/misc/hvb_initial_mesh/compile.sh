#!/bin/bash

MAIN=initial_mesh

# compile build.tex
#pdflatex -file-line-error -interaction=nonstopmode ${MAIN}.tex
lualatex ${MAIN}.tex
rm ${MAIN}.{aux,bbl,blg,synctex.gz,out,auxlock}

# vertically arrange figures 
#pdflatex -file-line-error -interaction=nonstopmode arrangeFigs.tex
#rm arrangeFigs.{aux,bbl,blg,synctex.gz}

# rename it
folderName=${PWD##*/}
mv ${MAIN}.pdf $folderName.pdf

# if multi-page figure
NB_PAGES=$(pdfinfo $folderName.pdf | grep Pages | awk '{print $2}')
if [ $NB_PAGES -gt 1 ]
then
  echo info: multi-page pdf found...
  echo "info: I'll split multi-page pdf file into individual images."
  pdfseparate ${folderName}.pdf ${folderName}_%d.pdf
fi


