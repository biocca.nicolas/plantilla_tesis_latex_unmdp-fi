# Plantilla Tesis LaTeX - UNMDP-FI

Plantilla de tesis escrita en LaTeX. Crea una clase 'unmdp' desde donde se modifican aspectos estéticos. Se crean environments personalizados para estructurar la escritura. Estructura los sources *.tex según capítulos y apéndices.

## Contribuciones

Esta plantilla es el resultado del trabajo de las siguientes personas, en orden cronológico. 

 - `Rodolfo Zeballos Salazar` (Universidad Nacional del Callao)
 - `dancab` (Universidad Nacional de Mar del Plata)
 - `nb` (Universidad Nacional de Mar del Plata)

Por supuesto, el proyecto está siempre abierto a contribuciones y serán bienvenidas.

## Características

Entre los cambios más importantes se encuentran:

- Se crea la clase `unmdp`.
- Se crean paquetes `package.sty` (`math_shortcuts.sty` y `my-comments.sty`) para la creación de comandos en ecuaciones y para introducir comentarios durante el proceso de escritura.
- Se utiliza el paquete `fncychap` para las portadas de los capítulos.
- Se definen `environment` para introducir fácilmente desde el `main` agradecimientos, resumen, abstract sin tener consideraciones en el formato, *just as plain text*.
- Define una estructura para la organización de todos los sources.
- Agrega un par de ejemplos para hacer gráficos con TikZ.
- Hace uso del paquete `microtype`, logrando una excelente tipografía en todo el documento. 


## Estructura

- unmdp.cls: clase referida a paquetes requeridos y formato del documento.
- main.tex: funciona como gestor de portada, agradecimientos, resumen, abstract, capítulos, apéndices. 
- math_shortcuts.sty: crea un paquete con comandos para facilitar la escritura de ecuaciones.
- definitions.sty
- references.bib
- `src/`: Contiene directorios referido a la redacción de cada de uno de los capítulos y apéndices. Además, contiene sources `*.tex` para agradecimientos, resumen, entre otros. 
   - `chapter??_chapter_name``: un directorio por capítulo. 
     - `img/`: contiene imágenes exclusivas del capítulo.
     - `misc/`: varios. Típicamente se utiliza este directorio para compilar figuras y luego enviarlas en formato *standalone* y luego enviarlas a `img`.
   - `chapter_name.tex`: un source por capítulo. 
   - `img/`: imágenes no alcanzadas por redacción de capítulos/apéndices. Por ejemplo, la portada.


## Getting started 

Una versión de este proyecto se encuentra montada en [Overleaf](https://www.overleaf.com).

[Link Aquí](https://www.overleaf.com/read/ryrjggtrqnbd)

## Preview

<img src="/uploads/8b97b838e59ed8bb31c60b4b446ac467/tesis_template.gif" width="600">

