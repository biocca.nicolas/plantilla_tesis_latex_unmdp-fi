% unmdpThesis LaTeX class 
% 
% Class written in the context of PhD program. 
% National University of Mar del Plata, Argentina.
%
% Author: N. Biocca <nicolas.biocca@fi.mdp.edu.ar>
% 
% unmdpThesis.cls is based on report (CTAN) class, released under 
% the LaTeX Project Public License.
%
% based on template provided by Daniel E. Caballero <dancab>
%


% Class name and version
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{unmdp}[2021/07/26 UNMDP Thesis class]


% load class
\LoadClass[12pt,a4paper,oneside]{report}


% require packages

%   LENGUAJE, FUENTE, ESCRITURA
\RequirePackage[spanish,es-tabla]{babel}    % soporte multilingue (Tabla instead of Cuadro)
\decimalpoint                               % force dot as decimal separator (needed when using spanish languaje)
\RequirePackage[T1]{fontenc}                % Standard package for selecting font encodings
\RequirePackage[utf8]{inputenc}             % Accept different input encodings
\RequirePackage{times}                      % Select Adobe Times Roman (or equivalent) as default font
\RequirePackage{textcomp}                   % simbolos de registrado y copyright
\RequirePackage{listings}                   % typeset programs (programming code)
\RequirePackage{lipsum}                     % dummy text to test out format of pages and the like
\RequirePackage{soul}                       % used just for strikethrough text. \st{} cmd.


%   PAGINAS, MARGENES, COLUMNAS
\RequirePackage[left=3.0cm, right=3.0cm, top=4.0cm, bottom=3.0cm]{geometry}
\RequirePackage{multicol}           % Intermix single and multiple columns
\RequirePackage{changepage}         % Margin adjustment and detection of odd/even pages
\RequirePackage{setspace}           % setear interlineado
\setstretch{1.3}
\RequirePackage{lscape}             % Place selected parts of a document in landscape
\RequirePackage{pdflscape}          % Make landscape pages display as landscape
\RequirePackage{color}              % color control (foreground (fonts, rules, etc) and background)
\RequirePackage{microtype}          % subliminal refinements towards typographical perfection
\RequirePackage{etoolbox}           % \restoregeometry
\AfterEndEnvironment{myenv}{\restoregeometry}
\interfootnotelinepenalty=10000     % force large footnotes not to be spread among the pages.


%    ECUACIONES Y ESCRITURA MATEMATICA
\RequirePackage[fleqn]{amsmath}      % AMS mathematical facilities (mathematical typesetting) ; fleqn: flush left equations
\RequirePackage{amssymb}
\setlength{\mathindent}{1.5cm}       % indentacion de las ecuaciones
\RequirePackage{amsfonts}            % mas fuentes de la AMS (mathcal entre otros)
\RequirePackage{mathrsfs}            % Support use of the Raph Smith’s Formal Script font in mathematics (mathscr, entre otros)
%\RequirePackage{dsfont}             % mathds: double stroke fonts for math environment
\RequirePackage{mathtools}           % Extension package for asmmath. Provide useful tool for mathematical typesetting.
\RequirePackage{xfrac}               % Split-level fractions (comando sfrac: fracciones bonitas tipo n/d)

%\RequirePackage{siunitx}             % A comprehensive (SI) units package
%\sisetup{per-mode=symbol}

\RequirePackage{mathcommand}         % define macros for math environment via \(re)newmathcommand

\RequirePackage[fleqn]{cases}        % numcases environment. Permite ecuaciones tipos cases numereadas.


%     FLOATS, FIGURAS, TABLAS, ENUMERACIONES, CROSS-REFERENCING
\RequirePackage{graphicx}            % Enhanced support for graphics
\RequirePackage{longtable}           % Allow tables to flow over page boundaries
\RequirePackage{float}               % Improved interface for floating objects
\RequirePackage{multirow}            % Create tabular cells spanning multiple rows
\RequirePackage{caption}             % Customising captions in floating environments
\RequirePackage{subcaption}          % Support for sub-captions
\RequirePackage{enumerate}           % Enumerate with redefinable labels
\RequirePackage{enumitem}            % Add control layout for list enviroments (enumerate, itemize and description) 
\RequirePackage{xcolor}              % Driver-independent color extensions
\RequirePackage{colortbl}            % Add color to LaTeX tables
\RequirePackage{hyperref}            % Extensive support for hypertext (cross-referencing)
\hypersetup{
    breaklinks = true,
    bookmarksopen=true,
    colorlinks=true,
    urlcolor=blue,
    linkcolor=blue,
    citecolor=blue,
}

\RequirePackage{bookmark}

\RequirePackage[spanish,ruled]{algorithm2e}

\RequirePackage{booktabs}


\RequirePackage[spanish]{cleveref}           % Intelligent cross-referencing.  Determine automatically the type of cross-reference (equation, section, etc.). Allow multiple eqref-like citations.
\crefrangeformat{subfigure}{#3#1#4--#5#2#6}



%   SECCIONES EXTRA: RESUMEN, ABSTRACT, GLOSARIO, APENDICES, BIBLIOGRAFIA
%\RequirePackage[refpages]{gloss}               % Create glossaries using BibTeX
\RequirePackage[numbers,sort&compress]{natbib}  % Flexible bibliography support
%\RequirePackage{bibentry}
\RequirePackage{appendix}                       % Extra control of appendices
\renewcommand{\appendixpagename}{Apéndices}
\renewcommand\appendixtocname{Apéndices}


%   ESTILO PORTADA DE CAPITULOS
%Options: Sonny, Lenny, Glenn, Conny, Rejne, Bjarne, Bjornstrup
\RequirePackage[Lenny]{fncychap}
\ChTitleVar{\LARGE\bfseries\rm}


% Workaround for properly index linking (not sure how it work).
% Magic command for properly links setup. Many thanks to stack over flow and specially to CyanideBaby.
% reference: https://tex.stackexchange.com/a/496321
\renewcommand*{\theHchapter}{\thepart.\thechapter} 


% interface to sectioning commands for selection from various title styles.
% This probably cames from <dancab> style.
\RequirePackage{titlesec}
\titleformat{\paragraph}{\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}
\titlespacing*{\paragraph}{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}



%
% COVER PAGE
%
\newcommand*{\university}[1]{\gdef\@university{#1}}          
\newcommand*{\faculty}[1]{\gdef\@faculty{#1}}
\newcommand*{\city}[1]{\gdef\@city{#1}}
\newcommand*{\country}[1]{\gdef\@country{#1}}

\newcommand*{\degree}[1]{\gdef\@degree{#1}}

\newcommand*{\advisorOne}[1]{\gdef\@advisorOne{#1}}
\newcommand*{\advisorTwo}[1]{\gdef\@advisorTwo{#1}}

\newcommand*{\coverPage}{
\newgeometry{top=2.0cm, bottom=2.0cm, left=2.0cm, right=2.0cm} 
\begin{titlepage}
    \begin{center}
        
        %\vfill
        \begin{figure}[H]
            \centering
            \includegraphics[width=1.0\textwidth]{src/img/logo_fi}
        \end{figure}
        
        
        \vspace{2cm}
        {\bf \huge \@title }\\[2cm] % titulo tesis
        
        % Author
        {{\bf \Large \@author} }\\[2.0cm]
        
        % degree
        {\large Tesis presentada para optar por el título de \\[0.5cm] \Large \@degree }%\\[2.25cm]
        
        \vfill
        
        \begin{itemize}[leftmargin=0pt,itemsep=0ex,parsep=0ex]
            \large
            \item[] Director: \@advisorOne
            \item[] Codirector: \@advisorTwo
        \end{itemize}
        
        \vfill
        
        {\large \@city, \@country}\\[0.2cm] % fecha de sustentación
        {\large \@date}
        
    \end{center}
\end{titlepage}
}

%
% Dedication page.
%
\newcommand*{\dedicationPage}[1]{
{\Large    
\begin{titlepage}
    \begin{flushright}
        \null \vspace {\stretch{1}}
        \textit{#1}
        \vspace {\stretch{2}}\null
    \end{flushright}
\end{titlepage}
}
}

\newenvironment{italic}
{\itshape}
{}

\newenvironment{dedication}[1][\Large]{
\begin{titlepage}
    #1 
    \begin{italic}
    \begin{flushright}
        \null \vspace {\stretch{1}}
        \ignorespaces
}
{
        \ignorespacesafterend \\        
        \vspace {\stretch{2}}\null
    \end{flushright}
    \end{italic}
\end{titlepage}
}

%
% acknowledges page
% 
\newenvironment{acknowledges}
{%
%\fontsize{10pt}{12pt}
%\setstretch{1.2}
\newgeometry{top=2.0cm, bottom=2.0cm, left=2.0cm, right=2.0cm}    
%\begin{titlepage}
\begin{center}
    {\Large \textbf{Agradecimientos}}
\end{center}
\par    % fuerza sangría inicial
}
{
%\end{titlepage}
\newpage\aftergroup\restoregeometry%
%\setstretch{1.3}
%\newgeometry{left=3.0cm, right=3.0cm, top=4.0cm, bottom=3.0cm}
\ignorespacesafterend
}


%
% Agradecimientos 
%
\newenvironment{agradecimientos}
{%
\phantomsection
\addcontentsline{toc}{chapter}{Agradecimientos}
\newgeometry{top=2.0cm, bottom=2.0cm, left=2.0cm, right=2.0cm} % formato pagina
\ChTitleVar{\centering\Large\bfseries\vspace{-5em}}
\chapter*{Agradecimientos}
\par\vspace{-5em}
}
{
\newpage\aftergroup\restoregeometry
\ignorespacesafterend
}

%
%
%
\newenvironment{resumen}
{%
    \phantomsection
    \addcontentsline{toc}{chapter}{Resumen}
    \fontsize{10pt}{12pt}
    \setstretch{1.2}
    \newgeometry{top=2.0cm, bottom=2.0cm, left=2.0cm, right=2.0cm} % formato pagina
    \ChTitleVar{\centering\Large\bfseries\vspace{-5em}}
    \chapter*{Resumen}
    \par\vspace{-5em}
}
{
    \newpage\aftergroup\restoregeometry
    \ignorespacesafterend
}

%
%
%
\renewenvironment{abstract}
{%
    \phantomsection
    \addcontentsline{toc}{chapter}{Abstract}
    \fontsize{10pt}{12pt}
    \setstretch{1.2}
    \newgeometry{top=2.0cm, bottom=2.0cm, left=2.0cm, right=2.0cm} % formato pagina
    \ChTitleVar{\centering\Large\bfseries\vspace{-5em}}
    \chapter*{Abstract}
    \par\vspace{-5em}
}
{
    \newpage\aftergroup\restoregeometry
    \ignorespacesafterend
}



%
% Generate pdf metadata
% 
\newcommand*{\generateMetadata}{
\hypersetup{
    pdftitle=\@title,
    pdfauthor=\@author,
    pdfsubject=Tesis Doctoral
    }   
}


%
% PRINT watermark
% 
\newif\ifwatermark
\watermarkfalse

\ifwatermark
    \RequirePackage{draftwatermark}
    \SetWatermarkText{DRAFT}     % default: DRAFT
    \SetWatermarkScale{1.0}      % default: 1.0
\fi





